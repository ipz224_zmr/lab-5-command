﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildersLibrary
{
    public class CharacterDirector
    {
        private readonly ICharacterBuilder _builder;

        public CharacterDirector(ICharacterBuilder builder)
        {
            _builder = builder;
        }

        public Character ConstructCharacter(string height, string build, string hairColor, string eyeColor, string clothing, List<string> inventory)
        {
            var builder = _builder
                .SetHeight(height)
                .SetBuild(build)
                .SetHairColor(hairColor)
                .SetEyeColor(eyeColor)
                .SetClothing(clothing);

            foreach (var item in inventory)
            {
                builder = builder.AddToInventory(item);
            }

            return builder.Build();
        }

    }

}
