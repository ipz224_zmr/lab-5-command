﻿namespace BuildersLibrary
{
    public class Character
    {
      
        public string Height { get; set; }
        public string Build { get; set; }
        public string HairColor { get; set; }
        public string EyeColor { get; set; }
        public string Clothing { get; set; }
        public List<string> Inventory { get; set; } = new List<string>();

        public void DisplayInfo()
        {
            Console.WriteLine($"Height: {Height}");
            Console.WriteLine($"Build: {Build}");
            Console.WriteLine($"Hair Color: {HairColor}");
            Console.WriteLine($"Eye Color: {EyeColor}");
            Console.WriteLine($"Clothing: {Clothing}");
            if (Inventory != null)
            {
                Console.WriteLine("Inventory:");
                foreach (var item in Inventory)
                {
                    Console.WriteLine($"-{item}");
                }
            }
        }
    }
}