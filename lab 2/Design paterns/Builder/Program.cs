﻿using BuildersLibrary;

class Program
{
    static void Main(string[] args)
    {
        var characterBuilder = new CharacterBuilderBase();
        var enemyBuilder = new EnemyBuilder();

        var director = new CharacterDirector(characterBuilder);
        var enemyDirector = new CharacterDirector(enemyBuilder);

        var hero = director.ConstructCharacter("Tall", "Athletic", "Blonde", "Blue", "Armor", new List<string> { "Sword", "Shield" });
        var enemy = enemyDirector.ConstructCharacter("Average", "Stocky", "Bald", "Red", "Robe", new List<string> { "Dark Magic" });

        Console.WriteLine("Hero:");
        hero.DisplayInfo();

        Console.WriteLine("\nEnemy:");
        enemy.DisplayInfo();
    }
}
