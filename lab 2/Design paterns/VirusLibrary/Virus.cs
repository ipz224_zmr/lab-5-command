﻿namespace VirusLibrary
{
    public class Virus : ICopyableVirus<Virus>
    {
        public string Name { get; set; }
        public int Weight { get; set; }
        public int Age { get; set; }
        public string Type { get; set; }
        public List<Virus> Children { get; set; }
        public Virus()
        {
            Children = new List<Virus>();
        }
        public Virus(string name, int weight, int age, string type, List<Virus> children)
        {
            Name = name;
            Weight = weight;
            Age = age;
            Type = type;
            Children = children;
        }

        public void PerformCopyTo(Virus obj)
        {
            obj.Name = Name;
            obj.Weight = Weight;
            obj.Age = Age;
            obj.Type = Type;
            obj.Children = new List<Virus>();
            foreach (var child in Children)
            {
                obj.Children.Add(child.PerformCopy());
            }
        }

        public override string ToString()
        {
            string childrenInfo = Children.Count > 0 ? $", Children: {string.Join(", ", Children)}" : "";
            return $"Name: {Name}, Weight: {Weight}, Age: {Age},\n Type: {Type}{childrenInfo} \n";
        }
    }
}