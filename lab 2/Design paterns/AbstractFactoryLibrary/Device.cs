﻿namespace AbstractFactoryLibrary
{
    public abstract class Device
    {
        private string Manufacturer { get; }

        public Device(string manufacturer)
        {
            this.Manufacturer = manufacturer;
        }

        public override string ToString()
        {
            return $"Manufacturer: {this.Manufacturer}, ";
        }
    }

}