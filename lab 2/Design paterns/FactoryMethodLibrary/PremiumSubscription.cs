﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethodLibrary
{
    public class PremiumSubscription : Subscription
    {
        public PremiumSubscription(List<string> IncludedChannels) : base(30.55m, 12, IncludedChannels) {}
    }

}
