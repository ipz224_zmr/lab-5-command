﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethodLibrary
{
    public class EducationalSubscription : Subscription
    {
        public EducationalSubscription(List<string> IncludedChannels) : base(16.66m, 6, IncludedChannels) { }
    }
}
