﻿namespace FactoryMethodLibrary
{
    public abstract class Subscription
    {
        public string SubscriberName { get; init; }
        public decimal MonthlyFee { get; protected set; }
        public int MinimumSubscriptionPeriod { get; protected set; }
        public List<string> IncludedChannels { get; protected set; }

        public Subscription(decimal monthlyFee, int minimumSubscriptionPeriod, List<string> includedChannels)
        {
            MonthlyFee = monthlyFee;
            MinimumSubscriptionPeriod = minimumSubscriptionPeriod;
            IncludedChannels = includedChannels;
        }
        public virtual void PrintSubscriptionInfo()
        {
            Console.WriteLine($"Class Name: {this.GetType().Name}");
            Console.WriteLine($"Monthly Fee: ${MonthlyFee}");
            Console.WriteLine($"Minimum Subscription Period: {MinimumSubscriptionPeriod} months");
            Console.WriteLine($"Included Channels: {string.Join(", ", IncludedChannels)} \n");
        }

    }
}