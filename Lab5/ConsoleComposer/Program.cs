﻿

using CommandClassLibrary;
using ComposerClassLibrary;

class Program
{
    static void Main(string[] args)
    {
        var textNode = new LightTextNode("This is a text node");
        var elementNode = new LightElementNode("div", "block", "normal", new List<string> { "class1", "class2" }, new List<LightNode> { textNode });
        Console.WriteLine($"{elementNode.InnerHTML}  \n {elementNode.OuterHTML} \n");
         textNode = new LightTextNode("\t Hello, world!");
        ICommand addNodeCommand = new AddNodeCommand(elementNode, textNode);
        addNodeCommand.Execute();
        Console.WriteLine($"{elementNode.InnerHTML}  \n {elementNode.OuterHTML} \n");
        ICommand RemoveCSSNodeCommand = new AddCssCommand(elementNode, "class2");
        RemoveCSSNodeCommand.Undo();
        RemoveCSSNodeCommand = new AddCssCommand(elementNode, "container");
        RemoveCSSNodeCommand.Execute();
        addNodeCommand.Undo();
        Console.WriteLine($"{elementNode.InnerHTML}  \n {elementNode.OuterHTML} \n");


    }
}