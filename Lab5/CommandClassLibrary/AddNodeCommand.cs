﻿using ComposerClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandClassLibrary
{
    public class AddNodeCommand : ICommand
    {
        private readonly LightElementNode _parentNode;
        private readonly LightNode _nodeToAdd;

        public AddNodeCommand(LightElementNode parentNode, LightNode nodeToAdd)
        {
            _parentNode = parentNode;
            _nodeToAdd = nodeToAdd;
        }

        public void Execute()
        {
         
            _parentNode.ChildNodes.Add(_nodeToAdd);
        }

        public void Undo()
        {
          
            _parentNode.ChildNodes.Remove(_nodeToAdd);
        }
    }

}
