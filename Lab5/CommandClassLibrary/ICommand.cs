﻿using ComposerClassLibrary;

namespace CommandClassLibrary
{
    public interface ICommand
    {
        void Execute();
        void Undo();
    }

}