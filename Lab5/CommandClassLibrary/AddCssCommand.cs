﻿using ComposerClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandClassLibrary
{
    public class AddCssCommand : ICommand
    {
        private readonly LightElementNode _elementNode;
        private readonly string _cssClass;

        public AddCssCommand(LightElementNode elementNode, string cssClass)
        {
            _elementNode = elementNode;
            _cssClass = cssClass;
        }

        public void Execute()
        {
          
            _elementNode.CSSClasses.Add(_cssClass);
        }

        public void Undo()
        {
        
            _elementNode.CSSClasses.Remove(_cssClass);
        }
    }

}
