﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyClassLibrary
{
    public class FilesystemImageLoadingStrategy : IImageLoadingStrategy
    {
        public byte[] LoadImage(string href)
        {
            return File.ReadAllBytes(href);
        }
    }
}
