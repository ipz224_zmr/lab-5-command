﻿using AgentClassLibrary;

class Program
{
    static void Main(string[] args)
    {
        List<Runway> runways = new List<Runway> { new Runway("Житомир-Лондон") , new Runway("Київ-Дубай") };

        var aircraft1 = new Aircraft("PlaneA");
        var aircraft2 = new Aircraft("PlaneB");
        List<Aircraft> aircrafts = new List<Aircraft> { aircraft1, aircraft2 };

        var commandCentre = new CommandCentre(runways, aircrafts);

        Console.WriteLine("Land and takeoff:");
        commandCentre.AircraftLandingInquiry(aircraft1);
        commandCentre.AircraftTakeOffInquiry(aircraft1);

        Console.WriteLine("Cant takeoff:");
        commandCentre.AircraftTakeOffInquiry(aircraft2);

        Console.WriteLine("Land and takeoff:");
        commandCentre.AircraftLandingInquiry(aircraft2);
        commandCentre.AircraftTakeOffInquiry(aircraft2);

        Console.WriteLine("Already landed:");
        commandCentre.AircraftLandingInquiry(aircraft1);
        commandCentre.AircraftLandingInquiry(aircraft1);

        Console.WriteLine("No available runways:");
        commandCentre.AircraftLandingInquiry(aircraft2);
        var aircraft3 = new Aircraft("PlaneC");
        commandCentre.AircraftLandingInquiry(aircraft3);
    }
}