﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MementoClassLibrary
{
    public class TextEditor
    {
        private TextDocument currentDocument;
        private HistoryManager historyManager;

        public TextEditor()
        {
            currentDocument = new TextDocument("");
            historyManager = new HistoryManager();
        }

        public void Save()
        {
            historyManager.Save(currentDocument);
        }

        public void Undo()
        {
            TextDocument previousDocument = historyManager.Undo();
            if (previousDocument != null)
            {
                currentDocument = previousDocument;
                Console.WriteLine("Undo successful.");
            }
            else
            {
                Console.WriteLine("Nothing to undo.");
            }
        }

        public void SetContent(string content)
        {
            currentDocument.Content = content;
        }

        public string GetContent()
        {
            return currentDocument.Content;
        }
    }

}
